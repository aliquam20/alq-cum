rootProject.name = "alq-cum"

pluginManagement {
    repositories {
        mavenLocal()
        maven("https://nexus.jeikobu.net/repository/maven-releases/")
        gradlePluginPortal()
    }
}