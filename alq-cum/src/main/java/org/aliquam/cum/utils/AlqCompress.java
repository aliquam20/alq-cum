package org.aliquam.cum.utils;

import lombok.SneakyThrows;
import org.apache.commons.compress.archivers.ArchiveEntry;
import org.apache.commons.compress.archivers.ArchiveInputStream;
import org.apache.commons.compress.archivers.ArchiveOutputStream;
import org.apache.commons.compress.archivers.tar.TarArchiveInputStream;
import org.apache.commons.compress.archivers.tar.TarArchiveOutputStream;
import org.apache.commons.compress.compressors.zstandard.ZstdCompressorInputStream;
import org.apache.commons.compress.compressors.zstandard.ZstdCompressorOutputStream;
import org.apache.commons.compress.utils.IOUtils;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.stream.Stream;

public class AlqCompress {

    private static String entryName(Path path) {
        Path root = path.subpath(0, 1);
        return path.toString().substring(root.toString().length() + 1);
    }

    public static void archiveDirectoryContent(Path sourceDir, Path archive) throws IOException {
        try (Stream<Path> filesToArchive = Files.walk(sourceDir);
             OutputStream fo = Files.newOutputStream(archive);
             OutputStream gzo = new ZstdCompressorOutputStream(fo); // Can optionally specify "level" parameter
//             OutputStream gzo = new GzipCompressorOutputStream(fo);
             ArchiveOutputStream aos = new TarArchiveOutputStream(gzo)) {
            filesToArchive
                    .filter(p -> !p.equals(sourceDir))
                    .forEach(p -> archiveSingleEntry(aos, p));
            aos.finish();
        }
    }

    @SneakyThrows
    public static void archiveSingleEntry(ArchiveOutputStream aos, Path p) {
        ArchiveEntry entry = aos.createArchiveEntry(p.toFile(), entryName(p));
        aos.putArchiveEntry(entry);
        if (Files.isRegularFile(p)) {
            try (InputStream is = Files.newInputStream(p)) {
                IOUtils.copy(is, aos);
            }
        }
        aos.closeArchiveEntry();
    }

    public static void extractToDirectory(Path archive, Path targetDir) throws IOException {

        try (InputStream fi = Files.newInputStream(archive);
             InputStream bi = new BufferedInputStream(fi);
             InputStream gzi = new ZstdCompressorInputStream(bi);
//             InputStream gzi = new GzipCompressorInputStream(bi);
             ArchiveInputStream ais = new TarArchiveInputStream(gzi)) {

            ArchiveEntry entry;
            while ((entry = ais.getNextEntry()) != null) {
                if (!ais.canReadEntryData(entry)) {
                    throw new IOException("Unsupported (or corrupted) entry");
                }
                Path targetEntryPath = targetDir.resolve(entry.getName());
                if (entry.isDirectory()) {
                    Files.createDirectories(targetEntryPath);
                } else {
                    Path parent = targetEntryPath.getParent();
                    Files.createDirectories(parent);
                    try (OutputStream os = Files.newOutputStream(targetEntryPath)) {
                        IOUtils.copy(ais, os);
                    }
                }
            }
        }
    }

}
