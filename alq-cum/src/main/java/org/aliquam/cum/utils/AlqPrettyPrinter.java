package org.aliquam.cum.utils;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.util.DefaultIndenter;
import com.fasterxml.jackson.core.util.DefaultPrettyPrinter;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

import java.io.IOException;

public class AlqPrettyPrinter extends DefaultPrettyPrinter {

    @Override
    public void writeObjectFieldValueSeparator(JsonGenerator jg) throws IOException {
        jg.writeRaw(": ");
    }

    @Override
    public DefaultPrettyPrinter createInstance() {
        AlqPrettyPrinter printer = new AlqPrettyPrinter();
        printer.indentArraysWith(DefaultIndenter.SYSTEM_LINEFEED_INSTANCE);
        printer.indentObjectsWith(DefaultIndenter.SYSTEM_LINEFEED_INSTANCE);
        return printer;
    }

    public static ObjectWriter getWriter() {
        return new ObjectMapper().writer(new AlqPrettyPrinter().createInstance());
    }

}