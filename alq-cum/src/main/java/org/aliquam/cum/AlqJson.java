package org.aliquam.cum;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

import java.io.InputStream;
import java.lang.reflect.Type;

@Slf4j
public class AlqJson {
    public static final ObjectMapper mapper = getMapper();

    public static ObjectMapper getMapper() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(new Jdk8Module());
        mapper.registerModule(new JavaTimeModule());
        return mapper;
    }

    public static JavaType constructType(Type t) {
        return mapper.constructType(t);
    }

    public static JavaType constructType(TypeReference<?> typeRef) {
        return mapper.constructType(typeRef);
    }

    public static <T> T parse(String json, Class<T> clazz) {
        return parse(json, mapper.constructType(clazz));
    }

    public static <T> T parse(String json, TypeReference<T> ref) {
        return parse(json, mapper.constructType(ref));
    }

    public static <T> T parse(String json, JavaType typeRef) {
        try {
//            log.info("Parsing JSON: " + json + " to " + typeRef);
            return mapper.readValue(json, typeRef);
        } catch (JsonProcessingException e) {
            throw new RuntimeException("JSON Parsing failed for [" + typeRef + "] " + json, e);
        }
    }

    @SneakyThrows
    public static <T> T parse(InputStream json, JavaType typeRef) {
        try {
            return mapper.readValue(json, typeRef);
        } catch (JsonProcessingException e) {
            throw new RuntimeException("JSON parsing into typeRef " + typeRef + " failed", e);
        }
    }

    public static String serialize(Object o) {
        try {
            return mapper
                    .writerWithDefaultPrettyPrinter()
                    .writeValueAsString(o);
        } catch (JsonProcessingException e) {
            throw new RuntimeException("Serializing " + o.getClass() + " class into JSON failed", e);
        }
    }

}
