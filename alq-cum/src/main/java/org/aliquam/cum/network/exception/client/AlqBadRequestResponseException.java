package org.aliquam.cum.network.exception.client;

import org.aliquam.cum.network.exception.UnsuccessfulResponseCodeException;

public class AlqBadRequestResponseException extends UnsuccessfulResponseCodeException {
    public static final int STATUS_CODE = 400;

    public AlqBadRequestResponseException() {
        super(STATUS_CODE);
    }
}
