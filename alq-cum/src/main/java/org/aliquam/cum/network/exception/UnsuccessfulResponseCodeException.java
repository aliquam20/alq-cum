package org.aliquam.cum.network.exception;

public class UnsuccessfulResponseCodeException extends RuntimeException {
    public final int statusCode;

    public UnsuccessfulResponseCodeException(int statusCode) {
        super("Server returned status code: " + statusCode);
        this.statusCode = statusCode;
    }
}
