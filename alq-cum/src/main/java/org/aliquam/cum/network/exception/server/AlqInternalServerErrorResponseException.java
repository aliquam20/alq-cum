package org.aliquam.cum.network.exception.server;

import org.aliquam.cum.network.exception.UnsuccessfulResponseCodeException;

public class AlqInternalServerErrorResponseException extends UnsuccessfulResponseCodeException {
    public static final int STATUS_CODE = 500;

    public AlqInternalServerErrorResponseException() {
        super(STATUS_CODE);
    }
}
