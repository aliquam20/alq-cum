package org.aliquam.cum.network.exception.client;

import org.aliquam.cum.network.exception.UnsuccessfulResponseCodeException;

public class AlqForbiddenResponseException extends UnsuccessfulResponseCodeException {
    public static final int STATUS_CODE = 401;

    public AlqForbiddenResponseException() {
        super(STATUS_CODE);
    }
}
