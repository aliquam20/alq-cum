package org.aliquam.cum.network.exception.client;

import org.aliquam.cum.network.exception.UnsuccessfulResponseCodeException;

public class AlqMethodNotAllowedResponseException extends UnsuccessfulResponseCodeException {
    public static final int STATUS_CODE = 405;

    public AlqMethodNotAllowedResponseException() {
        super(STATUS_CODE);
    }
}
