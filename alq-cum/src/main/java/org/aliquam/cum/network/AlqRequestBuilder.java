package org.aliquam.cum.network;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.aliquam.cum.AlqJson;
import org.aliquam.session.api.AlqSessionApi;
import org.aliquam.session.api.connector.AlqSessionManager;

import java.net.http.HttpRequest;
import java.util.Objects;

public class AlqRequestBuilder {

    private final HttpRequest.Builder request;
    private Object body;
    private boolean withRetries;

    @SneakyThrows
    public AlqRequestBuilder(AlqUrl url) {
        this.request = HttpRequest.newBuilder(url.toURI());
    }

    public AlqRequestBuilder acceptApplicationJson() {
        request.header("Accept", "application/json");
        return this;
    }

    public AlqRequestBuilder contentTypeApplicationJson() {
        request.header("Content-Type", "application/json");
        return this;
    }

    public AlqRequestBuilder includeSession(AlqSessionManager sessionManager) {
        request.header(AlqSessionApi.SESSION_HEADER, Objects.toString(sessionManager.getSessionId()));
        return this;
    }

    public AlqRequestBuilder withRetries() {
        withRetries = true;
        return this;
    }

    public AlqRequestBuilder header(String name, String value) {
        request.header(name, value);
        return this;
    }

    public AlqRequestBuilder GET() {
        request.GET();
        return this;
    }

    public AlqRequestBuilder POST(Object body) {
        return method("POST", body);
    }

    public AlqRequestBuilder PUT(Object body) {
        return method("PUT", body);
    }

    public AlqRequestBuilder PATCH(Object body) {
        return method("PATCH", body);
    }

    public AlqRequestBuilder DELETE() {
        request.DELETE();
        return this;
    }

    @SneakyThrows
    public AlqRequestBuilder method(String method, Object body) {
        String requestBody = AlqJson.serialize(body);
        contentTypeApplicationJson();
        request.method(method, HttpRequest.BodyPublishers.ofString(requestBody));
        return this;
    }

    public AlqRequest build() {
        return new AlqRequest(request.build(), body, withRetries);
    }
}
