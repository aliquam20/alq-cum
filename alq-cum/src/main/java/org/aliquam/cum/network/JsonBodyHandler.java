package org.aliquam.cum.network;

import com.fasterxml.jackson.databind.JavaType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.aliquam.cum.AlqJson;

import java.io.IOException;
import java.io.InputStream;
import java.io.UncheckedIOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.util.function.Supplier;

@Slf4j
public class JsonBodyHandler<T> implements HttpResponse.BodyHandler<Supplier<T>> {
    private final JavaType typeRef;

    public JsonBodyHandler(JavaType typeRef) {
        this.typeRef = typeRef;
    }

    private Supplier<T> toSupplierOfType(InputStream inputStream) {
        return () -> {
            try (InputStream stream = inputStream) {
                return AlqJson.parse(stream, typeRef);
            } catch (IOException e) {
                throw new UncheckedIOException(e);
            }
        };
    }

    private Supplier<T> toSupplierOfType(String string) {
        return () -> AlqJson.parse(string, typeRef);
    }

    private HttpResponse.BodySubscriber<Supplier<T>> asJSON() {
        return HttpResponse.BodySubscribers.mapping(
//                HttpResponse.BodySubscribers.ofInputStream(), // Switched to String because it can be easily logged if something goes wrong
                HttpResponse.BodySubscribers.ofString(StandardCharsets.UTF_8),
                this::toSupplierOfType);
    }

    @Override
    public HttpResponse.BodySubscriber<Supplier<T>> apply(HttpResponse.ResponseInfo responseInfo) {
        return asJSON();
    }

}