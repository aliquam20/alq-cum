package org.aliquam.cum.network;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.type.TypeFactory;
import lombok.Getter;
import org.aliquam.cum.AlqJson;

import java.net.URI;
import java.net.http.HttpRequest;

public class AlqRequest {

    @Getter
    private final HttpRequest request;

    @Getter
    private final Object body;

    @Getter
    private final boolean withRetries; // TODO: Retry policy?

    public AlqRequest(HttpRequest request, Object body, boolean withRetries) {
        this.request = request;
        this.body = body;
        this.withRetries = withRetries;
    }

    public String getMethod() {
        return request.method();
    }

    public URI getUri() {
        return request.uri();
    }

    public void execute() {
        AlqConnector.execute(this);
    }

    public <T> T execute(Class<T> responseType) {
        return AlqConnector.execute(this, AlqJson.constructType(responseType));
    }

    public <T> T execute(TypeReference<T> responseType) {
        return AlqConnector.execute(this, AlqJson.constructType(responseType));
    }

    public <T> T execute(JavaType type) {
        return AlqConnector.execute(this, type);
    }
}
