package org.aliquam.cum.network.exception.client;

import org.aliquam.cum.network.exception.UnsuccessfulResponseCodeException;

public class AlqUnauthorizedResponseException extends UnsuccessfulResponseCodeException {
    public static final int STATUS_CODE = 401;

    public AlqUnauthorizedResponseException() {
        super(STATUS_CODE);
    }
}
