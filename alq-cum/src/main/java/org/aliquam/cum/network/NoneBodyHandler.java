package org.aliquam.cum.network;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.io.InputStream;
import java.io.UncheckedIOException;
import java.net.http.HttpResponse;
import java.util.function.Supplier;

public class NoneBodyHandler implements HttpResponse.BodyHandler<Supplier<Void>> {

    public NoneBodyHandler() {
    }

    private static Supplier<Void> toSupplierOfType(InputStream inputStream) {
        return () -> {
            try (InputStream stream = inputStream) {
                return null;
            } catch (IOException e) {
                throw new UncheckedIOException(e);
            }
        };
    }

    private static HttpResponse.BodySubscriber<Supplier<Void>> asJSON() {
        return HttpResponse.BodySubscribers.mapping(
                HttpResponse.BodySubscribers.ofInputStream(),
                NoneBodyHandler::toSupplierOfType);
    }

    @Override
    public HttpResponse.BodySubscriber<Supplier<Void>> apply(HttpResponse.ResponseInfo responseInfo) {
        return NoneBodyHandler.asJSON();
    }

}