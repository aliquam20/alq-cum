package org.aliquam.cum.network.exception.server;

import org.aliquam.cum.network.exception.UnsuccessfulResponseCodeException;

public class AlqNotImplementedResponseException extends UnsuccessfulResponseCodeException {
    public static final int STATUS_CODE = 501;

    public AlqNotImplementedResponseException() {
        super(STATUS_CODE);
    }
}
