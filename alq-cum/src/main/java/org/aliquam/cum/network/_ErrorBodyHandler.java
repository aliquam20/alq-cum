package org.aliquam.cum.network;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.file.Path;

class _ErrorBodyHandler {

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class AlqResponse<R, E>{
        private R response;
        private E error;
    }

    static class ErrorBodyHandler<R,T> implements HttpResponse.BodyHandler<AlqResponse<R,T>> {
        final HttpResponse.BodyHandler<R> responseHandler;
        final HttpResponse.BodyHandler<T> errorHandler;
        public ErrorBodyHandler(HttpResponse.BodyHandler<R> responseHandler, HttpResponse.BodyHandler<T> errorHandler) {
            this.responseHandler = responseHandler;
            this.errorHandler = errorHandler;
        }
        @Override
        public HttpResponse.BodySubscriber<AlqResponse<R, T>> apply(HttpResponse.ResponseInfo responseInfo) {
            if (responseInfo.statusCode() == 200) {
                return HttpResponse.BodySubscribers.mapping(responseHandler.apply(responseInfo),
                        (r) -> new AlqResponse<>(r, null));
            } else {
                return HttpResponse.BodySubscribers.mapping(errorHandler.apply(responseInfo),
                        (t) -> new AlqResponse<>(null, t));
            }
        }
    }

    public static void main(String[] args) throws Exception {
        var client = HttpClient.newHttpClient();
        var handler = new ErrorBodyHandler<>(
                HttpResponse.BodyHandlers.ofFileDownload(Path.of(".")),
                HttpResponse.BodyHandlers.ofString()
        );
        var request = HttpRequest
                .newBuilder(URI.create("http://host:port/"))
                .build();
        var httpResponse =
                client.send(request, handler);
        if (httpResponse.statusCode() == 200) {
            Path path = httpResponse.body().getResponse();
        } else {
            String error = httpResponse.body().getError();
        }
    }

}
