package org.aliquam.cum.network.exception.client;

import org.aliquam.cum.network.exception.UnsuccessfulResponseCodeException;

public class AlqNotFoundResponseException extends UnsuccessfulResponseCodeException {
    public static final int STATUS_CODE = 404;

    public AlqNotFoundResponseException() {
        super(STATUS_CODE);
    }
}
