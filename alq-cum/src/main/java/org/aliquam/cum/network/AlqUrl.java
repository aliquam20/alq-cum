package org.aliquam.cum.network;

import lombok.SneakyThrows;

import java.net.URI;

public class AlqUrl {

    private final String urlStr;
    private final URI uri;

    @SneakyThrows
    public AlqUrl(String url) {
        this.urlStr = url;
        this.uri = new URI(url);
    }

    @SneakyThrows
    public AlqUrl(AlqUrl parent, String pathPart) {
        if(parent.urlStr.endsWith("/")) {
            urlStr = parent.urlStr + pathPart;
        } else {
            urlStr = parent.urlStr + "/" + pathPart;
        }
        this.uri = new URI(urlStr);
    }

    public AlqUrl resolve(String... parts) {
        String joined = String.join("/", parts);
        return new AlqUrl(this, joined);
    }

    public URI toURI() {
        return uri;
    }

    @Override
    public String toString() {
        return urlStr;
    }
}
