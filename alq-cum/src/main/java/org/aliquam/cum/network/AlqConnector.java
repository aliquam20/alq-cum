package org.aliquam.cum.network;

import com.fasterxml.jackson.databind.JavaType;
import io.github.resilience4j.retry.Retry;
import io.github.resilience4j.retry.RetryConfig;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.aliquam.cum.network.exception.UnsuccessfulResponseCodeException;
import org.aliquam.cum.network.exception.UnsuccessfulResponseCodeExceptionFactory;

import java.net.http.HttpClient;
import java.net.http.HttpResponse;
import java.util.concurrent.Callable;
import java.util.function.Supplier;

@Slf4j
public class AlqConnector {
    private static final AlqUrl GATEWAY_URL = new AlqUrl(System.getenv("ALQ_GATEWAY"));
    private static final HttpClient client = HttpClient.newHttpClient();

    public static AlqUrl of(String servicePath) {
        return GATEWAY_URL.resolve(servicePath, "api");
    }

    public static void execute(AlqRequest request) { // TODO: responseType not needed?
        execute(request, new NoneBodyHandler());
    }

    public static <T> T execute(AlqRequest request, JavaType responseType) { // TODO: responseType not needed?
        return execute(request, new JsonBodyHandler<T>(responseType));
    }

    @SneakyThrows
    public static <T> T execute(AlqRequest request, HttpResponse.BodyHandler<Supplier<T>> bodyHandler) {
        if(request.isWithRetries()) {
            return withRetries(() -> executeSingle(request, bodyHandler));
        } else {
            return executeSingle(request, bodyHandler);
        }
    }

    @SneakyThrows
    public static <T> T executeSingle(AlqRequest request, HttpResponse.BodyHandler<Supplier<T>> bodyHandler) {
        log.info(">>> Calling {} {}", request.getMethod(), request.getUri());

        HttpResponse<Supplier<T>> responseSupplier = client.send(request.getRequest(), bodyHandler);
        if(responseSupplier.statusCode() < 200 || responseSupplier.statusCode() >= 300) {
            throw UnsuccessfulResponseCodeExceptionFactory.create(responseSupplier.statusCode());
        }
        T response = responseSupplier.body().get();

        log.info("<<< Response: [{}] {}", response == null ? null : response.getClass(), response);
        return response;
    }

//    public static void checkStatus(Response response, Response.Status... expectedStatuses) {
//        boolean statusOk = Arrays.stream(expectedStatuses)
//                .anyMatch(expStatus -> response.getStatus() == expStatus.getStatusCode());
//        if(!statusOk) {
//            throw new RuntimeException("Unexpected reponse status: " + response.getStatus() + ", " +
//                    "Expected statuses: " + Arrays.toString(expectedStatuses));
//        }
//    }

    public static <T> T withRetries(Callable<T> request) throws Exception {
        RetryConfig retryConfig = RetryConfig.custom()
                .maxAttempts(7)
                .intervalFunction(numAttempts -> numAttempts * 1000L)
                // If server returned an error code then retrying the request (probably) wouldn't change much
                // This policy is rather focused on retrying due to cases like:
                // - Random network issues (lost connection, timeouts)
                // - Service being down (crashes/restarting)
                .ignoreExceptions(UnsuccessfulResponseCodeException.class)
                .build();
        Retry retry = Retry.of("AlqConnector call", retryConfig);

        retry.getEventPublisher()
                .onRetry(event -> log.info("Call failed but will be retried in {}", event.getWaitInterval()));

        Callable<T> decoratedRequest = Retry.decorateCallable(retry, request);
        return decoratedRequest.call();
    }
}
