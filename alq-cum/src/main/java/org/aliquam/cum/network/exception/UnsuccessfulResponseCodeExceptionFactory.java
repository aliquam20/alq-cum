package org.aliquam.cum.network.exception;

import org.aliquam.cum.network.exception.client.AlqBadRequestResponseException;
import org.aliquam.cum.network.exception.client.AlqForbiddenResponseException;
import org.aliquam.cum.network.exception.client.AlqMethodNotAllowedResponseException;
import org.aliquam.cum.network.exception.client.AlqNotAcceptableResponseException;
import org.aliquam.cum.network.exception.client.AlqNotFoundResponseException;
import org.aliquam.cum.network.exception.client.AlqUnauthorizedResponseException;
import org.aliquam.cum.network.exception.server.AlqInternalServerErrorResponseException;
import org.aliquam.cum.network.exception.server.AlqNotImplementedResponseException;

public class UnsuccessfulResponseCodeExceptionFactory {

    public static UnsuccessfulResponseCodeException create(int statusCode) {
        switch(statusCode) {
            case 400: return new AlqBadRequestResponseException();
            case 401: return new AlqUnauthorizedResponseException();
            case 403: return new AlqForbiddenResponseException();
            case 404: return new AlqNotFoundResponseException();
            case 405: return new AlqMethodNotAllowedResponseException();
            case 406: return new AlqNotAcceptableResponseException();
            case 500: return new AlqInternalServerErrorResponseException();
            case 501: return new AlqNotImplementedResponseException();
            default: return new UnsuccessfulResponseCodeException(statusCode);
        }
    }

}
