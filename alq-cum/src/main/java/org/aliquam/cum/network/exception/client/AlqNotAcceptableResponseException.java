package org.aliquam.cum.network.exception.client;

import org.aliquam.cum.network.exception.UnsuccessfulResponseCodeException;

public class AlqNotAcceptableResponseException extends UnsuccessfulResponseCodeException {
    public static final int STATUS_CODE = 406;

    public AlqNotAcceptableResponseException() {
        super(STATUS_CODE);
    }
}
