package org.aliquam.cum.messaging;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DeliverCallback;
import lombok.SneakyThrows;
import org.aliquam.cum.AlqJson;

import java.nio.charset.StandardCharsets;
import java.util.function.Consumer;

public class QueueConnector {

    @SneakyThrows
    public static Connection connect() {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost(System.getenv("ALQ_MSG_BROKER_HOST"));
        factory.setPort(Integer.parseInt(System.getenv("ALQ_MSG_BROKER_PORT")));
        factory.setVirtualHost("/");
        factory.setUsername(System.getenv("ALQ_MSG_BROKER_USER"));
        factory.setPassword(System.getenv("ALQ_MSG_BROKER_PASS"));

        return factory.newConnection();
    }

    @SneakyThrows
    public static <T> void subscribe(Connection connection, String queueName, Class<T> consumerClazz, Consumer<T> consumer) {
        Channel channel = connection.createChannel();
        channel.queueDeclare(queueName, true, false, false, null);

        DeliverCallback deliverCallback = (consumerTag, delivery) -> {
            String messageStr = new String(delivery.getBody(), StandardCharsets.UTF_8);
            T message = AlqJson.parse(messageStr, consumerClazz);
            consumer.accept(message);
        };

        channel.basicConsume(queueName, true, deliverCallback, consumerTag -> { });
    }

}
