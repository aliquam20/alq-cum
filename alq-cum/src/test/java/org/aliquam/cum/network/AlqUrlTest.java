package org.aliquam.cum.network;

import org.junit.jupiter.api.Test;

import java.net.URI;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

class AlqUrlTest {

    @Test
    public void testAlqUrl() {

        AlqUrl url = new AlqUrl("http://localhost:123/test");
        AlqUrl url2 = url.resolve("xxx", "yyy", "zzz");
        URI uri = url2.toURI();
        assertThat(uri.toString(), is("http://localhost:123/test/xxx/yyy/zzz"));
    }

}